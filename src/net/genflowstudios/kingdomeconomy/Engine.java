package net.genflowstudios.kingdomeconomy;

import net.genflowstudios.kingdomeconomy.commands.CommandManager;
import net.genflowstudios.kingdomeconomy.data.ConfigManager;
import net.genflowstudios.kingdomeconomy.data.OfferLoader;
import net.genflowstudios.kingdomeconomy.listeners.PlayerJoinQuitListener;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Samuel on 6/17/2015.
 */
public class Engine extends JavaPlugin implements Listener{


    private static Engine instance;


    public static Engine getInstance(){
        return instance;
    }

    /**
     * Create a new pop-up menu and stores it for later use
     *
     * @param title The menu title
     * @param rows  The number of rows on the menu
     * @return The menu
     */
    public static Menu createMenu(String title, int rows){
        return new Menu(title, rows);
    }

    /**
     * Creates an exact copy of an existing pop-up menu. This is intended to be
     * used for creating dynamic pop-up menus for individual players. Be sure to
     * call destroyMenu for menus that are no longer needed.
     *
     * @param menu The menu to clone
     * @return The cloned copy
     */
    public static Menu cloneMenu(Menu menu){
        return menu.clone();
    }

    /**
     * Destroys an existing menu, and closes it for any viewers
     * <p>
     * Please note: you should not store any references to destroyed menus
     *
     * @param menu The menu to destroy
     */
    public static void removeMenu(Menu menu){
        for(HumanEntity viewer : menu.getInventory().getViewers()){
            if(viewer instanceof Player){
                menu.closeMenu((Player) viewer);
            }else{
                viewer.closeInventory();
            }
        }
    }

    /**
     * Due to a bug with inventories, switching from one menu to another in the
     * same tick causes glitchiness. In order to prevent this, the opening must
     * be done in the next tick. This is a convenience method to perform this
     * task for you.
     *
     * @param player   The player switching menus
     * @param fromMenu The menu the player is currently viewing
     * @param toMenu   The menu the player is switching to
     */
    public static void switchMenu(final Player player, Menu fromMenu, final Menu toMenu){
        fromMenu.closeMenu(player);
        new BukkitRunnable(){
            @Override
            public void run(){
                toMenu.openMenu(player);
            }
        }.runTask(instance);
    }

    @Override
    public void onEnable(){
        instance = this;
        getLogger().info("has enabled");
        CommandManager commandManager = new CommandManager(this);
        getCommand("Market").setExecutor(commandManager);
        getServer().getPluginManager().registerEvents(this, this);
        getServer().getPluginManager().registerEvents(new PlayerJoinQuitListener(), this);
        checkFiles();
        Bukkit.getScheduler().runTaskLater(this, new Runnable(){
            @Override
            public void run(){
                OfferLoader offerLoader = new OfferLoader();
                offerLoader.registerOfflineOffers();
            }
        }, 50);
    }

    @Override
    public void onDisable(){
        getLogger().info("has disabled");
    }

    public void checkFiles(){
        File kingdomFiles = new File(getInstance().getDataFolder(), "Settings");
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        if(kingdomFiles.exists() == false){
            cfg.createSection("Settings");
            cfg.getConfigurationSection("Settings").set("Default-Time", 84600);
            cfg.createSection("Players");
            cfg.getConfigurationSection("Players").set("UUIDS", new ArrayList<String>());
            ConfigManager.saveDefaultConfig(cfg);
        }else{

        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onMenuItemClicked(InventoryClickEvent event){
        Inventory inventory = event.getInventory();
        if(inventory.getHolder() instanceof Menu){
            Menu menu = (Menu) inventory.getHolder();
            if(event.getWhoClicked() instanceof Player){
                Player player = (Player) event.getWhoClicked();
                if(event.getSlotType() == InventoryType.SlotType.OUTSIDE){
                    // Quick exit for a menu, click outside of it
                    if(menu.exitOnClickOutside()){
                        menu.closeMenu(player);
                    }
                }else{
                    int index = event.getRawSlot();
                    if(index < inventory.getSize()){
                        menu.selectMenuItem(player, index);
                    }else{
                        // If they want to mess with their inventory they don't need to do so in a menu
                        if(menu.exitOnClickOutside()){
                            menu.closeMenu(player);
                        }
                    }
                }
            }
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onMenuClosed(InventoryCloseEvent event){
        if(event.getPlayer() instanceof Player){
            Inventory inventory = event.getInventory();
            if(inventory.getHolder() instanceof Menu){
                Menu menu = (Menu) inventory.getHolder();
                MenuCloseBehaviour menuCloseBehaviour = menu.getMenuCloseBehaviour();
                if(menuCloseBehaviour != null){
                    menuCloseBehaviour.onClose((Player) event.getPlayer());
                }
            }
        }
    }


}
