package net.genflowstudios.kingdomeconomy.objects;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Samuel on 6/28/2015.
 */
public class BuyerManager{

    public ArrayList<Buyer> buyers = new ArrayList<Buyer>();
    public HashMap<Buyer, ArrayList<Offer>> boughtOffers = new HashMap<Buyer, ArrayList<Offer>>();


    public void addBuyer(Buyer buyer) throws Exception{
        if(doesBuyerExist(buyer)){
            throw new Exception("That buyer already exists!");
        }else{
            buyers.add(buyer);
            boughtOffers.put(buyer, new ArrayList<Offer>());
        }
    }

    public void addBoughtOffer(Buyer buyer, Offer offer) throws Exception{
        if(doesBuyerExist(buyer)){
            if(doesOfferExist(buyer, offer)){
                throw new Exception("That offer already exists!");
            }else{
                boughtOffers.get(buyer).add(offer);
            }
        }else{
            throw new Exception("That buyer does not exist!");
        }
    }

    public void removeBoughtOffer(Buyer buyer, Offer offer) throws Exception{
        if(doesBuyerExist(buyer)){
            if(doesOfferExist(buyer, offer)){
                boughtOffers.get(buyer).remove(offer);
            }else{
                throw new Exception("That offer does not exist!");
            }
        }else{
            throw new Exception("That buyer does not exist!");
        }
    }

    public void removeBuyer(Buyer buyer) throws Exception{
        if(doesBuyerExist(buyer)){
            buyers.remove(buyer);
        }else{
            throw new Exception("That buyer does not exist!");
        }
    }

    private boolean doesBuyerExist(Buyer buyer){
        return !(buyers.isEmpty() || buyers.contains(buyer) == false);
    }


    private boolean doesOfferExist(Buyer buyer, Offer offer){
        return !(boughtOffers.get(buyer).isEmpty() || boughtOffers.get(buyer).contains(offer) == false);
    }


}
