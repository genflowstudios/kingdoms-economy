package net.genflowstudios.kingdomeconomy.objects;

import net.genflowstudios.kingdomeconomy.data.ConfigManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Samuel on 6/28/2015.
 */
public class OfferManager{


    public static HashMap<Vendor, ArrayList<Offer>> offers = new HashMap<Vendor, ArrayList<Offer>>();

    public void addOffer(Vendor vendor, Offer offer) throws Exception{
        if(offers.isEmpty() == false && offers.containsKey(vendor)){
            if(offers.containsValue(offer)){
                throw new Exception("That offer already exists!");
            }else{
                offers.get(vendor).add(offer);
                sendOffer(offer);
            }
        }else{
            offers.put(vendor, new ArrayList<Offer>());
            offers.get(vendor).add(offer);
            sendOffer(offer);
        }
    }

    public void removeOffer(Vendor vendor, Offer offer) throws Exception{
        if(offers.isEmpty() == false && offers.containsKey(vendor)){
            ArrayList<Offer> offers2 = (ArrayList<Offer>) getOffers(vendor).clone();
            for(Offer o : offers2){
                if(o.getItem().equals(offer.getItem())){
                    offers.get(vendor).remove(offer);
                }
            }
        }else{
            throw new Exception("That vendor does not exist!");
        }
    }

    public ArrayList<Offer> getOffers(Vendor vendor) throws Exception{
        if(offers.isEmpty() == false && offers.containsKey(vendor)){
            return offers.get(vendor);
        }else{
            offers.put(vendor, new ArrayList<Offer>());
            return offers.get(vendor);
        }
    }

    public void sendOffer(Offer offer){
        FileConfiguration cfg = ConfigManager.getPlayerConfig(offer.getVendor());
        int offers = cfg.getInt("Current Offers");
        int id = offers + 1;
        cfg.getConfigurationSection("Offers").createSection("" + id);
        if(offer.getItem().hasItemMeta()){
            if(offer.getItem().getItemMeta().hasDisplayName()){
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Material", offer.getItem().getType().toString());
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Name", offer.getItem().getItemMeta().getDisplayName());
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Lore", getLore(offer.getItem()));
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Amount", offer.getAmount());
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Cost", offer.getCost());
                cfg.set("Current Offers", id);
            }else{
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Material", offer.getItem().getType().toString());
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Name", capitalizeFirstLetter(offer.getItem().getType().toString().replace("_", " ").toLowerCase()));
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Lore", getLore(offer.getItem()));
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Amount", offer.getAmount());
                cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Cost", offer.getCost());
                cfg.set("Current Offers", id);
            }
        }else{
            cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Material", offer.getItem().getType().toString());
            cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Name", capitalizeFirstLetter(offer.getItem().getType().toString().replace("_", " ").toLowerCase()));
            cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Lore", getLore(offer.getItem()));
            cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Amount", offer.getAmount());
            cfg.getConfigurationSection("Offers").getConfigurationSection("" + id).set("Item-Cost", offer.getCost());
            cfg.set("Current Offers", id);
        }
        ConfigManager.savePlayerConfig(offer.getVendor(), cfg);


    }

    public String getLore(ItemStack item){
        String lore = "";
        if(item.hasItemMeta()){
            if(item.getItemMeta().hasLore()){
                for(String s : item.getItemMeta().getLore()){
                    lore.concat("~" + s);
                }
            }
        }
        return lore;
    }

    public String capitalizeFirstLetter(String original){
        if(original.length() == 0)
            return original;
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }
}
