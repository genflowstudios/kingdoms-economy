package net.genflowstudios.kingdomeconomy.objects;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 6/28/2015.
 */
public class VendorManager{

    public static ArrayList<Vendor> vendors = new ArrayList<Vendor>();


    public void addVendor(Vendor vendor) throws Exception{
        if(doesVendorExist(vendor) == false){
            vendors.add(vendor);
        }else{
            throw new Exception("That vendor already exists!");
        }
    }

    public void removeVendor(Vendor vendor) throws Exception{
        if(vendors.isEmpty() || vendors.contains(vendor) == false){
            throw new Exception("That vendor does not exist!");
        }else{
            vendors.remove(vendor);
        }
    }


    public boolean doesVendorExist(Vendor vendor){
        if(vendors.isEmpty()){
            return false;
        }else{
            return vendors.contains(vendor);

        }
    }

    public Vendor getVendor(UUID uuid) throws Exception{
        if(vendors.isEmpty()){
            throw new Exception("That vendor does not exist!");
        }else{
            for(Vendor vendor : getVendors()){
                if(vendor.getUuid().equals(uuid)){
                    return vendor;
                }
            }
            throw new Exception("That vendor does not exist!");
        }
    }

    public ArrayList<Vendor> getVendors(){
        return vendors;
    }
}
