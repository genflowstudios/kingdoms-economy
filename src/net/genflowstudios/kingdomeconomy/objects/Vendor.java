package net.genflowstudios.kingdomeconomy.objects;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 6/28/2015.
 */
public class Vendor{


    UUID uuid;
    ArrayList<Offer> offers;

    public Vendor(UUID uuid){
        OfferManager offerManager = new OfferManager();
        VendorManager vendorManager = new VendorManager();
        this.uuid = uuid;
        this.offers = new ArrayList<Offer>();
        try{
            vendorManager.addVendor(this);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void addOffer(ItemStack item, int amount, double cost){
        OfferManager offerManager = new OfferManager();
        try{
            Offer offer = new Offer(getUuid(), item, amount, cost);
            offerManager.addOffer(this, offer);
            getOffers().add(offer);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void removeOffer(Material type){
        OfferManager offerManager = new OfferManager();
        ArrayList<Offer> offers2 = (ArrayList<Offer>) getOffers().clone();
        for(Offer offer : offers2){
            if(offer.getItem().getType().equals(type)){
                try{
                    offerManager.removeOffer(this, offer);
                    offers.remove(offer);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    public Offer getOffer(Material type) throws Exception{
        for(Offer offer : getOffers()){
            if(offer.getItem().getType().equals(type)){
                return offer;
            }
        }
        throw new Exception("That offer does not exist!");
    }

    public UUID getUuid(){
        return uuid;
    }

    public void setUuid(UUID uuid){
        this.uuid = uuid;
    }

    public ArrayList<Offer> getOffers(){
        return offers;
    }

    public void setOffers(ArrayList<Offer> offers){
        this.offers = offers;
    }
}
