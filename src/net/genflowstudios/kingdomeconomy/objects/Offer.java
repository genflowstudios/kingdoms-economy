package net.genflowstudios.kingdomeconomy.objects;

import net.genflowstudios.kingdomeconomy.data.SettingsHandler;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

/**
 * Created by Samuel on 6/28/2015.
 */
public class Offer{

    UUID vendor;
    ItemStack item;
    int amount;
    double cost;
    int timeLeft;
    Offer offer;

    public Offer(UUID vendor, ItemStack item, int amount, double cost){
        SettingsHandler settingsHandler = new SettingsHandler();
        this.vendor = vendor;
        this.item = item;
        this.item.setAmount(amount);
        this.amount = amount;
        this.cost = cost;
        this.timeLeft = settingsHandler.getDefaultTimeWait();
        this.offer = this;
    }

    public UUID getVendor(){
        return vendor;
    }

    public void setVendor(UUID vendor){
        this.vendor = vendor;
    }

    public ItemStack getItem(){
        return item;
    }

    public void setItem(ItemStack item){
        this.item = item;
    }

    public int getAmount(){
        return amount;
    }

    public void setAmount(int amount){
        this.amount = amount;
    }

    public double getCost(){
        return cost;
    }

    public void setCost(double cost){
        this.cost = cost;
    }

    public int getTimeLeft(){
        return timeLeft;
    }

    public void setTimeLeft(int timeLeft){
        this.timeLeft = timeLeft;
    }
}
