package net.genflowstudios.kingdomeconomy.data;

import net.genflowstudios.kingdomeconomy.Engine;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.logging.Level;

public class ConfigManager{

    static Engine kingdom;

    //Returns default config.
    public static FileConfiguration getDefaultConfig(){
        String fileName = "config" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves default config.
    public static void saveDefaultConfig(FileConfiguration cfg){
        String fileName = "config" + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Settings");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }


    //Returns player config.
    public static FileConfiguration getPlayerConfig(UUID name){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Players");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves player config.
    public static void savePlayerConfig(UUID uuid, FileConfiguration cfg){
        String fileName = uuid + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Players");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }


    //Returns city config.
    public static FileConfiguration getCityConfig(String name){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Cities");
        File configFile = new File(dataFolder, fileName);
        return YamlConfiguration.loadConfiguration(configFile);
    }

    //Saves city config.
    public static void saveCityConfig(String name, FileConfiguration cfg){
        String fileName = name + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Cities");
        dataFolder.mkdirs();
        File configFile = new File(dataFolder, fileName);
        try{
            cfg.save(configFile);
        }catch(IOException ex){
            Engine.getInstance().getLogger().log(Level.SEVERE, "Could not save config to " + configFile, ex);
        }
    }

}
