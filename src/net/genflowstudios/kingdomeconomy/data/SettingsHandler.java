package net.genflowstudios.kingdomeconomy.data;

import net.genflowstudios.kingdomeconomy.Engine;
import org.bukkit.configuration.file.FileConfiguration;

public class SettingsHandler{

    Engine kingdoms;


    public boolean isFlatfileMode(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getBoolean("Flatfile");
    }

    public boolean isVaultEnabled(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getConfigurationSection("Plugin-Hook-Settings").getBoolean("Vault-Enabled");
    }

    public int getDefaultTimeWait(){
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        return cfg.getConfigurationSection("Settings").getInt("Default-Time");
    }

}
