package net.genflowstudios.kingdomeconomy.data;

import net.genflowstudios.kingdomeconomy.objects.Offer;
import net.genflowstudios.kingdomeconomy.objects.Vendor;
import net.genflowstudios.kingdomeconomy.objects.VendorManager;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Samuel on 6/29/2015.
 */
public class OfferLoader{

    ArrayList<Offer> offers = new ArrayList<Offer>();

    public void registerOfflineOffers(){
        VendorManager vendorManager = new VendorManager();
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        for(String s : cfg.getConfigurationSection("Players").getStringList("UUIDS")){
            UUID player = UUID.fromString(s);
            Vendor vendor = new Vendor(player);
            FileConfiguration cfgPlayer = ConfigManager.getPlayerConfig(player);
            if(cfgPlayer.getInt("Current Offers") == 0){

            }else{
                for(int i = 1; i <= cfgPlayer.getInt("Current Offers"); i++){
                    Material material = getMaterial(cfgPlayer.getConfigurationSection("Offers").getConfigurationSection("" + i).getString("Item-Material"));
                    String itemName = cfgPlayer.getConfigurationSection("Offers").getConfigurationSection("" + i).getString("Item-Name");
                    int itemAmount = cfgPlayer.getConfigurationSection("Offers").getConfigurationSection("" + i).getInt("Item-Amount");
                    double itemCost = cfgPlayer.getConfigurationSection("Offers").getConfigurationSection("" + i).getDouble("Item-Cost");
                    ItemStack item = new ItemStack(material, itemAmount);
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(itemName);
                    item.setItemMeta(meta);
                    if(doesOfferExist(vendor, item)){

                    }else{
                        vendor.addOffer(item, itemAmount, itemCost);
                    }
                }
            }
        }
    }

    public boolean doesOfferExist(Vendor vendor, ItemStack item){
        for(Offer o : vendor.getOffers()){
            if(o.getItem().equals(item)){
                return true;
            }
        }
        return false;
    }

    public ArrayList<Material> getMaterials(FileConfiguration cfgPlayer){
        ArrayList<Material> materials = new ArrayList<Material>();
        for(int i = 1; i <= cfgPlayer.getInt("Current Offers"); i++){
            Material material = getMaterial(cfgPlayer.getConfigurationSection("Offers").getConfigurationSection("" + i).getString("Item-Material"));
            materials.add(material);
        }
        return materials;
    }

    public Material getMaterial(String name){
        for(Material material : Material.values()){
            if(material.toString().equals(name)){
                return material;
            }
        }
        return Material.AIR;
    }

}
