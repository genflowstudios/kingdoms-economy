package net.genflowstudios.kingdomeconomy.data;

import net.genflowstudios.kingdomeconomy.Engine;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;
import java.util.UUID;

public class ConfigChangeHandler{

    static Engine kingdoms;


    public void generatePlayerConfig(UUID uuid, FileConfiguration cfgPlayer){
        cfgPlayer.set("Player-Name", Bukkit.getPlayer(uuid).getName());
        cfgPlayer.set("Current Offers", 0);
        cfgPlayer.createSection("Offers");
        ConfigManager.savePlayerConfig(uuid, cfgPlayer);
        FileConfiguration cfg = ConfigManager.getDefaultConfig();
        List<String> uuids = cfg.getConfigurationSection("Players").getStringList("UUIDS");
        uuids.add(uuid.toString());
        cfg.getConfigurationSection("Players").set("UUIDS", uuids);
        ConfigManager.saveDefaultConfig(cfg);
    }


    public void savePlayerConfigs(UUID uuid){
        FileConfiguration cfg = net.genflowstudios.kingdoms.data.ConfigManager.getPlayerConfig(uuid);

    }


}
