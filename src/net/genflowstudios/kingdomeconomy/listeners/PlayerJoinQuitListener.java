package net.genflowstudios.kingdomeconomy.listeners;


import net.genflowstudios.kingdomeconomy.Engine;
import net.genflowstudios.kingdomeconomy.data.ConfigChangeHandler;
import net.genflowstudios.kingdomeconomy.data.ConfigManager;
import net.genflowstudios.kingdomeconomy.objects.Vendor;
import net.genflowstudios.kingdomeconomy.objects.VendorManager;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.io.File;
import java.util.UUID;

/**
 * Created by Samuel on 6/28/2015.
 */
public class PlayerJoinQuitListener implements Listener{


    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
        final UUID uuid = player.getUniqueId();
        if(doesVendorExist(player.getUniqueId())){
            checkFiles(player, new ConfigChangeHandler());
        }else{
            Vendor vendor = new Vendor(player.getUniqueId());
            checkFiles(player, new ConfigChangeHandler());
        }
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event){
        Player player = event.getPlayer();

    }


    public boolean doesVendorExist(UUID uuid){
        VendorManager vendorManager = new VendorManager();
        for(Vendor vendor : vendorManager.getVendors()){
            if(vendor.getUuid().equals(uuid)){
                return true;
            }
        }
        return false;
    }


    private void checkFiles(Player player, ConfigChangeHandler changeHandler){
        String fileName = player.getUniqueId() + ".yml";
        File dataFolder = new File(Engine.getInstance().getDataFolder(), "Players");
        File configFile = new File(dataFolder, fileName);
        if(configFile.exists()){

        }else{
            FileConfiguration cfgPlayer = ConfigManager.getPlayerConfig(player.getUniqueId());
            changeHandler.generatePlayerConfig(player.getUniqueId(), cfgPlayer);
        }
    }


}
