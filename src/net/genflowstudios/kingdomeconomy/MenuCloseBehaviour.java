package net.genflowstudios.kingdomeconomy;

import org.bukkit.entity.Player;

/**
 * Created by Samuel on 6/29/2015.
 */
public interface MenuCloseBehaviour{

    /**
     * Called when a player closes a menu
     *
     * @param player The player closing the menu
     */
    void onClose(Player player);
}
