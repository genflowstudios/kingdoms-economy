package net.genflowstudios.kingdomeconomy.commands;

import net.genflowstudios.kingdomeconomy.Engine;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandManager implements CommandExecutor{

    Engine kingdoms;
    String error = "===[" + ChatColor.AQUA + "Kingdoms" + ChatColor.WHITE + "]===\n" + ChatColor.RED + "[ERROR] - That command does not exist!";

    //Constructor
    public CommandManager(Engine kingdoms){
        this.kingdoms = kingdoms;
    }

    //On player command.
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(Label.equalsIgnoreCase("Market")){
            new MarketCommand(sender, cmd, Label, args);
        }else{

        }
        return false;
    }


}
