package net.genflowstudios.kingdomeconomy.commands;

import net.genflowstudios.kingdomeconomy.Engine;
import net.genflowstudios.kingdomeconomy.Menu;
import net.genflowstudios.kingdomeconomy.MenuItem;
import net.genflowstudios.kingdomeconomy.data.ConfigManager;
import net.genflowstudios.kingdomeconomy.objects.Offer;
import net.genflowstudios.kingdomeconomy.objects.OfferManager;
import net.genflowstudios.kingdomeconomy.objects.Vendor;
import net.genflowstudios.kingdomeconomy.objects.VendorManager;
import net.genflowstudios.kingdoms.player.KingdomPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.UUID;

public class MarketCommand{

    Menu vendors = Engine.createMenu("Online Vendors", 6);
    Menu vendorOffers = Engine.createMenu("Vendor Offers", 3);

    public MarketCommand(CommandSender sender, Command cmd, String Label, String[] args){
        if(sender instanceof Player){
            Player player = (Player) sender;
            if(Label.equalsIgnoreCase("Market")){
                if(args.length == 0){
                    infoCommand(player, cmd, Label, args);
                }else if(args.length == 1){
                    if(args[0].equalsIgnoreCase("Stats")){
                        statsCommand(player, cmd, Label, args);
                    }else if(args[0].equalsIgnoreCase("Buy")){
                        buyCommand(player, cmd, Label, args);
                    }
                }else if(args.length == 2){
                    if(args[0].equalsIgnoreCase("Create")){
                        createCommand(player, cmd, Label, args);
                    }
                }
            }
        }

    }

    private static void sendInfoMessage(Player player){
        player.sendMessage("===[" + ChatColor.AQUA + "Market" + ChatColor.WHITE + "]===\n" + ChatColor.GREEN + "List generic information regarding kingdom markets.");
    }

    private void buyCommand(Player player, Command cmd, String label, String[] args){
        final UUID uuid = player.getUniqueId();
        Bukkit.getScheduler().runTaskLater(Engine.getInstance(), new Runnable(){
            @Override
            public void run(){
                final VendorManager vendorManager = new VendorManager();
                final OfferManager offerManager = new OfferManager();
                int id = 0;
                if(vendorManager.getVendors().isEmpty()){
                    getOnlineVendors().addMenuItem(new MenuItem(ChatColor.DARK_RED + "There are currently no online vendors.", new ItemStack(Material.BARRIER, 1).getData(), 1){
                        @Override
                        public void onClick(Player p){
                            getOnlineVendors().closeMenu(p);
                        }
                    }, id);
                }else{
                    populateVendors(vendorManager, uuid, id);
                }

            }
        }, 60);
        getOnlineVendors().openMenu(player);
    }

    private void createCommand(Player player, Command cmd, String label, String[] args){
        VendorManager vendorManager = new VendorManager();
        try{
            Vendor vendor = vendorManager.getVendor(player.getUniqueId());
            vendor.addOffer(player.getItemInHand(), player.getItemInHand().getAmount(), Double.parseDouble(args[1]));
            player.sendMessage("You have successfully created a new offer for " + player.getItemInHand().getAmount() + " " + player.getItemInHand().getType().toString() + " at a cost of " + Double.parseDouble(args[1]) + " bonds.");
            player.getInventory().remove(player.getItemInHand());
        }catch(Exception e){
            e.printStackTrace();
        }


    }

    private void statsCommand(Player player, Command cmd, String label, String[] args){

    }

    private void infoCommand(Player player, Command cmd, String label, String[] args){
        sendInfoMessage(player);
    }

    public String capitalizeFirstLetter(String original){
        if(original.length() == 0)
            return original;
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }


    public Menu getOnlineVendors(){
        return vendors;
    }

    public Menu getVendorOffers(){
        return vendorOffers;
    }

    public int getID(Offer offer){
        UUID uuid = offer.getVendor();
        FileConfiguration cfg = ConfigManager.getPlayerConfig(uuid);
        for(int i = 1; i <= cfg.getInt("Current Offers"); i++){
            Material material = getMaterial(cfg.getConfigurationSection("Offers").getConfigurationSection("" + i).getString("Item-Material"));
            if(material.equals(offer.getItem().getType().equals(material))){
                return i;
            }
        }
        return 0;
    }

    public ArrayList<Material> getMaterials(FileConfiguration cfgPlayer){
        ArrayList<Material> materials = new ArrayList<Material>();
        for(int i = 1; i <= cfgPlayer.getInt("Current Offers"); i++){
            Material material = getMaterial(cfgPlayer.getConfigurationSection("Offers").getConfigurationSection("" + i).getString("Item-Material"));
            materials.add(material);
        }
        return materials;
    }

    public Material getMaterial(String name){
        for(Material material : Material.values()){
            if(material.toString().equals(name)){
                return material;
            }
        }
        return Material.AIR;
    }

    public void populateOffers(OfferManager offerManager, final Vendor vendor, int id2){
        int temp = id2;
        try{
            for(final Offer offer : offerManager.getOffers(vendor)){
                getVendorOffers().addMenuItem(new MenuItem(ChatColor.RED + "Offer: " + capitalizeFirstLetter(offer.getItem().getType().name().replace("_", " ").toLowerCase()) + ChatColor.GREEN + " Cost: " + offer.getCost() + " bonds.", offer.getItem().getData(), offer.getAmount()){
                    @Override
                    public void onClick(Player p){
                        try{
                            KingdomPlayer kingdomPlayer = KingdomPlayer.getPlayer(p.getUniqueId());
                            if(kingdomPlayer.getStats().getBonds() >= offer.getCost()){
                                kingdomPlayer.getStats().setBonds(kingdomPlayer.getStats().getBonds() - offer.getCost());
                                p.getInventory().addItem(offer.getItem());
                                p.sendMessage("You have successfully bought " + offer.getAmount() + " " + capitalizeFirstLetter(offer.getItem().getType().name().replace("_", " ").toLowerCase()) + " for " + offer.getCost() + " bonds.");
                                vendor.removeOffer(offer.getItem().getType());
                                FileConfiguration cfg = ConfigManager.getPlayerConfig(offer.getVendor());
                                cfg.getConfigurationSection("Offers").set("" + getID(offer), null);
                                getVendorOffers().closeMenu(p);
                                ConfigManager.savePlayerConfig(offer.getVendor(), cfg);
                            }else{
                                p.sendMessage("You do not have enough bonds to complete this transaction");
                                getVendorOffers().closeMenu(p);
                            }
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                }, temp);
                temp += 1;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public void populateVendors(VendorManager vendorManager, UUID uuid, int id){
        int temp = id;
        for(final Vendor vendor : vendorManager.getVendors()){
            if(vendor.getUuid().equals(uuid)){
                temp -= 1;
            }else{
                getOnlineVendors().addMenuItem(new MenuItem(ChatColor.AQUA + "Vendor: " + Bukkit.getOfflinePlayer(vendor.getUuid()).getName() + ChatColor.GOLD + " Current Offers: " + vendor.getOffers().size(), new ItemStack(Material.GOLD_INGOT, 1).getData(), 1){
                    @Override
                    public void onClick(Player p){
                        OfferManager offerManager = new OfferManager();
                        try{
                            if(offerManager.getOffers(vendor).isEmpty()){
                                getVendorOffers().addMenuItem(new MenuItem(ChatColor.BLUE + "Go Back", new ItemStack(Material.SIGN, 1).getData(), 1){
                                    @Override
                                    public void onClick(Player p){
                                        getOnlineVendors().switchMenu(p, getOnlineVendors());
                                    }
                                }, 26);
                            }else{
                                int id2 = 0;
                                getVendorOffers().addMenuItem(new MenuItem(ChatColor.BLUE + "Go Back", new ItemStack(Material.SIGN, 1).getData(), 1){
                                    @Override
                                    public void onClick(Player p){
                                        getOnlineVendors().switchMenu(p, getOnlineVendors());
                                    }
                                }, 26);
                                populateOffers(offerManager, vendor, id2);
                            }

                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        getOnlineVendors().switchMenu(p, getVendorOffers());
                    }
                }, temp);
            }
            temp += 1;
        }
    }

}
